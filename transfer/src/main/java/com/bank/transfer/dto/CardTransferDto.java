package com.bank.transfer.dto;

import com.bank.transfer.entity.CardTransferEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ДТО {@link CardTransferEntity}
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CardTransferDto implements Serializable {
    @NotNull
    @Positive(message = "Значение поля 'id' должно быть положительным числом")
    Long id;
    @NotNull
    @Positive(message = "Значение поля 'cardNumber' должно быть положительным числом")
    @DecimalMin(value = "1000000000000000",
            message = "Значение поля 'cardNumber' должно содержать не менее 16 цифр")
    @DecimalMax(value = "9999999999999999",
            message = "Значение поля 'cardNumber' должно содержать не более 16 цифр")
    Long cardNumber;
    @NotNull
    @Positive(message = "Значение поля 'amount' должно быть положительным числом")
    BigDecimal amount;
    @NotBlank(message = "Поле 'purpose' не может быть пустым или содержать только пробелы")
    String purpose;
    @NotNull
    @Positive(message = "Значение поля 'accountDetailsId' должно быть положительным числом")
    Long accountDetailsId;
}
