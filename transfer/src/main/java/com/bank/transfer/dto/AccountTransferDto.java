package com.bank.transfer.dto;

import com.bank.transfer.entity.AccountTransferEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ДТО {@link AccountTransferEntity}
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountTransferDto implements Serializable {
    @NotNull
    @Positive(message = "Значение поля 'id' должно быть положительным числом")
    Long id;
    @NotNull
    @Min(10000000L)
    @Max(999999999999L)
    @Positive(message = "Значение поля 'accountNumber' должно быть положительным числом")
    Long accountNumber;
    @NotNull
    @Positive(message = "Значение поля 'amount' должно быть положительным числом")
    BigDecimal amount;
    @NotBlank(message = "Поле 'purpose' не может быть пустым или содержать только пробелы")
    String purpose;
    @NotNull
    @Positive(message = "Значение поля 'accountDetailsId' должно быть положительным числом")
    Long accountDetailsId;
}
